package com.company;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    private final static String fileName = "./files/inputFile.txt";
    private final static String outputFileName = "./files/outputFile.txt";

    public static void main(String[] args) {

        String textString = readFileToString(fileName);

        if (!textString.isEmpty()) {
            TreeMap<String, Integer> wordsCountedMap = countWords(textString);

            System.out.println("1. Вывести результаты в консоль");
            System.out.println("2. Записать результаты в файл");

            Short answer = 1;

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                answer = Short.parseShort(br.readLine());
            }
            catch (Exception exception) {
                System.out.println("Не удалось считать ответ. Результаты выведутся в консоль");
            }

            //Все кроме двойки выведет в консоль.
            //Сделать можно по разному, вплоть до цикла, который не выводит результаты, пока клиент не нажмет 1 или 21
            if (answer == 2)
                writeTreeMapToFile(wordsCountedMap, outputFileName);
            else
                writeTreeMapToConsole(wordsCountedMap);

        }
        else
            System.out.print("Входящий файл пуст");
    }

   public static String readFileToString (String fileName){

        Path path = Paths.get(fileName);

        StringBuilder sb = new StringBuilder();

        try (Scanner scanner =  new Scanner(path)){
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
                sb.append('\n');
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static TreeMap<String, Integer> countWords(String textString){

        //TreeMap автоматически сортирует ключи. Удобно для слов
        TreeMap<String, Integer> resultMap = new TreeMap<>();

        // Оставляю только буквы, цифры и whitespace-ы
        // перевожу в нижний регистр, потому что Word и word одно слово, а так же удобнее все это будет читать на мой взгляд
        textString = textString.replaceAll("[^a-zA-Z0-9\\s]+", "").toLowerCase();

        //Разбиваю на слова по любым whitespace-ам
        String[] words = textString.split("\\s+");

        //Если ключ-слово есть, беру значение, если нет, по дефолту беру 0. Прибавляю к счетчику 1
        for (String word : words)
            resultMap.put(word, resultMap.getOrDefault(word, 0) + 1);

        return resultMap;
    }

    private static void writeTreeMapToFile(TreeMap<String, Integer> treeMap, String fileName){

        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8)){
            for(Map.Entry<String,Integer> entry : treeMap.entrySet()) {
                bufferedWriter.write(entry.getKey() + " => " + entry.getValue());
                bufferedWriter.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeTreeMapToConsole(TreeMap<String, Integer> treeMap){
        for(Map.Entry<String,Integer> entry : treeMap.entrySet())
            System.out.println(entry.getKey() + " => " + entry.getValue());
    }

}
