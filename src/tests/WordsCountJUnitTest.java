package tests;

import com.company.Main;
import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WordsCountJUnitTest {

    private static String testFileName = "./files/testFile.txt";
    private static String testString = "A test file to test everything\n";

    @Test
    void testReadFromFile(){
        String resultTextString = Main.readFileToString(testFileName);
        assertEquals(testString, resultTextString);
    }

    @Test
    void testWordsCount(){
        TreeMap<String, Integer> resultTreeMap = Main.countWords(testString);
        assertEquals((Integer) 1, resultTreeMap.get("everything"));
        assertEquals((Integer) 2, resultTreeMap.get("test"));
    }
}